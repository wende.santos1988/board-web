import Head from 'next/head'

import styles from '../styles/styles.module.scss'
import { doesNotMatch } from 'assert'

export default function Home() { 

    const imgLink = 'https://avatars.githubusercontent.com/u/14131685?s=40&v=4'

    return (
        <>
            <Head>
                <title>Welcome board-app</title>
            </Head>
            <main className={styles.contentContainer}>
                <img src='/images/board-user.svg' alt='board tool'/>

                <section className={styles.callToAction}>
                    <h1>A tool to your day by day, Write, Planne, Get organized ...</h1>
                    <p><span>100% Free</span> and online.</p>
                </section>

                <div className={styles.donaters}>
                    <img src={imgLink} alt="" />
                </div>
            </main>
        </>
        
    )

}
