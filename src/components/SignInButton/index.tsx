import { FaGithub } from 'react-icons/fa'
import { FiX } from 'react-icons/fi'

import styles from './styles.module.scss'

export const SignInButton = () => {

    const imgLink = 'https://avatars.githubusercontent.com/u/14131685?s=40&v=4'
    const session = false;

    return session ? (
        <button type='button' className={styles.signInButton} onClick={() => {}}>
            <img src={imgLink} alt='Wendel Santos'></img>
            Hi, Wendel
            <FiX color='#737380' className={styles.closeIcon}/>
        </button>
    ) : (
        <button type='button' className={styles.signInButton} onClick={() => {}}>
            <FaGithub color='#FFB800'/>
            Login with github
        </button>
    )
}